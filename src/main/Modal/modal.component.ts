import { Component, Injectable, ViewChild, ContentChild, TemplateRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'popup',
    templateUrl: './modal.html'
})

export class ModalComponent {

    public successValue: boolean = false;
    public errorValue: boolean = false;
    public message: string;
    private redirect: string;
    public showResult: boolean = false;
    public popupvalues = {};

    constructor(private router: Router) { }

    public getSuccess(succes: string, redirect: string) {
        this.message = succes;
        this.successValue = true;
        this.redirect = redirect;
    }

    public getError(error: string) {
        this.message = error;
        this.errorValue = true;

    }

    public showDetails(values) {
        this.popupvalues = values;
        this.showResult = true;
    }

    hideError() {
        this.message = null;
        this.errorValue = false;
        this.successValue = false;
        this.router.navigate(['/apply']);
        
    }


    hideModal() {
        this.message = null;
        this.errorValue = false;
        this.successValue = false;
        this.showResult = false;
        
    }
}