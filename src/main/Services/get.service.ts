import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Principal } from '../shared/auth/principal.service';
import { contentHeaders } from '../shared/shared.headers';
import { AuthServerProvider } from '../shared/auth/auth-jwt.service';
import { HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable()
export class GetService {

    constructor(
        private principal: Principal,
        private authServerProvider: AuthServerProvider,
        private http: Http
    ) { }

    emailCheck(value: string) {
        return this.http.get(environment.apiUrl + '/api/register/emailCheck?email=' + value)
            .map((response: Response) => response.json())
    }

    getActiveJobs() {
        return this.http.get(environment.apiUrl + '/api/register/active/jobs')
            .map((response: Response) => response.json())
    }
    getPersonalDetails(value: any) {
        return this.http.get(environment.apiUrl + '/api/register/personal/' + value)
            .map((response: Response) => response.json())
    }
    getState() {
        return this.http.get(environment.apiUrl + '/api/register/state')
            .map((response: Response) => response.json())
    }

    getCity(cityId: string) {
        return this.http.get(environment.apiUrl + '/api/register/city/' + cityId)
            .map((response: Response) => response.json())
    }

    getBoard() {
        return this.http.get(environment.apiUrl + '/api/register/boards')
            .map((response: Response) => response.json())
    }

    getUniversity() {
        return this.http.get(environment.apiUrl + '/api/register/university')
            .map((response: Response) => response.json())
    }

    getCourse(type: string) {
        return this.http.get(environment.apiUrl + '/api/register/courses/type?type=' + type)
            .map((response: Response) => response.json())
    }

    getCategory() {
        return this.http.get(environment.apiUrl + '/api/register/category')
            .map((response: Response) => response.json())
    }

    getYears() {
        return this.http.get(environment.apiUrl + '/api/register/year')
            .map((response: Response) => response.json())
    }

    getFormFields(value: any) {
        return this.http.get(environment.apiUrl + '/api/register/form/fields/' + value)
            .map((response: Response) => response.json())
    }

    getAmountFields(value: any) {
        return this.http.get(environment.apiUrl + '/api/register/form/amount/' + value)
            .map((response: Response) => response.json())
    }

    getCreatedJobs() {
        return this.http.get(environment.apiUrl + '/api/admin/total/jobs')
            .map((response: Response) => response.json())
    }
    getCandidateList() {
        return this.http.get(environment.apiUrl + '/api/register/personal')
            .map((response: Response) => response.json())
    }
}