import { Injectable } from '@angular/core';
//import {LocalStorage, SessionStorage} from "angular2-localstorage/WebStorage";
@Injectable()
export class Principal {
    //@SessionStorage()
    //public _isAuthenticated: boolean = false;
    // @SessionStorage()
    //public _roles: Array<string> = [];
    // @SessionStorage()
    //public _fullName: string = null;


    public get isAuthenticated(): string {
        return localStorage.getItem('isAuthenticated');
    }

    public get roles(): string {
        return localStorage.getItem('roles');
    }

    public get getFullName(): string {
        return localStorage.getItem('name');
    }


    public set setFullName(v: string) {
        localStorage.setItem('name', v);
    }

    public set roles(v: string) {
        localStorage.setItem('roles', v);
    }

    public set isAuthenticated(v: string) {
        localStorage.setItem('isAuthenticated', v);
    }
    public clearAll() {
        localStorage.clear();
    }
    public hasRole(role: string): boolean {  
        if (!this.isAuthenticated || !this.roles) {
            return false;
        }
        return this.roles.indexOf(role) > -1;
    }

    public hasAnyRole(roles: Array<string>): boolean { 
        if (!this.isAuthenticated || !this.roles) {
            return false;
        }
        for (let i = 0; i < roles.length; i++) {
            if (this.hasRole(roles[i])) {
                return true;
            }
        }
        return false;
    }



}