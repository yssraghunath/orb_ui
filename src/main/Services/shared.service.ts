import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Principal } from '../shared/auth/principal.service';
import { contentHeaders } from '../shared/shared.headers';
import { AuthServerProvider } from '../shared/auth/auth-jwt.service';
import { HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable()
export class SharedService {

    public sharingData: any;
    public sharingJob: any;
    public sharingRegisterId:any;
    
    constructor(
        private principal: Principal,
        private authServerProvider: AuthServerProvider,
        private http: Http
    ) { }

    saveData(str) {
        this.sharingData = str;
    }
    getData() {
        return this.sharingData;
    }

     saveJobData(str) {
        this.sharingJob = str;
    }

    //by dheeraj
    editJobData(str) {
        this.sharingJob = str;
    }


     getJobData() {
        return this.sharingJob;
    }

    saveRegisterData(str){
        this.sharingRegisterId = str;
    }
    getRegisterId() {
        return this.sharingRegisterId;
    }
}