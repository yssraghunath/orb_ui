import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    constructor(private http: Http) { }

    login(username: string, password: string) {
        return this.http.post('http://localhost:8080/api/authenticate', JSON.stringify({ username: username, password: password }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    save(mail: string): Observable<any> {
        return this.http.post('http://localhost:8080/api/account/reset_password/init', mail);
    }

    saveJob(value: string): Observable<any> {
        return this.http.post('http://localhost:8080/api/admin/save/job', value);
    }

    saveJobFields(value: any): Observable<any> {
        return this.http.post('http://localhost:8080/api/admin/save/job/fields', value);
    }

    removeJob(value: string) {
        return this.http.post('http://localhost:8080/api/admin/remove/', value);
    }


    editJob(value: string) {
        return this.http.post('http://localhost:8080/api/admin/edit/', value);
    }

    applyFilter(value: any): Observable<any> {
        return this.http.post('http://localhost:8080/api/register/personal/filter', value);
    }

    adminRegister(admin: any) {
        return this.http.post('http://localhost:8080/api/admin/register/', admin);
    }
    createCandidate(user: any): Observable<any> {
        let headers = new Headers();
        headers.append('enctype', 'multipart/form-data');
        let options = new RequestOptions({ headers: headers });

        let formData = new FormData();
        formData.append("recruitmentZone", user.recruitmentZone);
        formData.append("registrationId", user.registrationId);
        formData.append("jobId", user.jobId);
        formData.append("payment", user.payment);
        formData.append("firstName", user.firstName);
        formData.append("middleName", user.middleName);
        formData.append("lastName", user.lastName);
        formData.append("fatherName", user.fatherName);
        formData.append("motherName", user.motherName);
        formData.append("category", user.category);
        formData.append("categoryCertificateDoc", user.categoryCertificateDoc);
        var datestr = (new Date(user.dateOfBirth)).toDateString();
        formData.append("dateOfBirth", (new Date(user.dateOfBirth)).toDateString());
        formData.append("age", user.age);
        formData.append("gender", user.gender);
        formData.append("finalSubmit", user.finalSubmit);
        formData.append("mobileNumber", user.mobileNumber);
        formData.append("emailAddress", user.emailAddress);
        formData.append("nationality", user.nationality);
        formData.append("aadhaarCard", user.aadhaarCard);
        formData.append("aadhaarNumber", user.aadhaarNumber);
        formData.append("aadhaarCertificateDoc", user.aadhaarCertificateDoc);
        formData.append("maritalStatus", user.maritalStatus);
        formData.append("belongCommunity", user.belongCommunity);
        formData.append("communityName", user.communityName);
        formData.append("permanentAddress", user.permanentAddress);
        formData.append("permanentState", user.permanentState);
        formData.append("permanentCity", user.permanentCity);
        formData.append("permanentPincode", user.permanentPincode);
        formData.append("postalAddress", user.postalAddress);
        formData.append("postalState", user.postalState);
        formData.append("postalCity", user.postalCity);
        formData.append("postalPincode", user.postalPincode);

        formData.append("centralGovt", user.centralGovt);
        formData.append("nocCertificateDoc", user.nocCertificateDoc);
        formData.append("servedFrom", (new Date(user.servedFrom)).toDateString());
        formData.append("servedTo", (new Date(user.servedTo)).toDateString());


        formData.append("sonDaughterExservice", user.sonDaughterExservice);
        formData.append("armyNumber", user.armyNumber);
        formData.append("armyRank", user.armyRank);
        formData.append("sonDaughterGref", user.sonDaughterGref);
        formData.append("sonDaughterNumber", user.sonDaughterNumber);
        formData.append("sonDaughterRank", user.sonDaughterRank);
        formData.append("sonDaughterName", user.sonDaughterName);
        formData.append("brotherSisterGref", user.brotherSisterGref);
        formData.append("brotherSisterNumber", user.brotherSisterNumber);
        formData.append("brotherSisterRank", user.brotherSisterRank);
        formData.append("brotherSisterName", user.brotherSisterName);
        formData.append("nccBCertificate", user.nccBCertificate);
        formData.append("nccBCertificateDoc", user.nccBCertificateDoc);
        formData.append("nccCCertificate", user.nccCCertificate);
        formData.append("nccCCertificateDoc", user.nccCCertificateDoc);
        formData.append("sportsMan", user.sportsMan);
        formData.append("sportsManDoc", user.sportsManDoc);
        formData.append("highYear", user.highYear);
        formData.append("highInstitution", user.highInstitution);
        formData.append("highSpecialization", user.highSpecialization);
        formData.append("highCourseType", user.highCourseType);
        formData.append("highBoard", user.highBoard);
        formData.append("highTotalMarks", user.highTotalMarks);
        formData.append("highObtainMarks", user.highObtainMarks);
        formData.append("highPercentage", user.highPercentage);
        formData.append("highCertificateDoc", user.highCertificateDoc);
        formData.append("interYear", user.interYear);
        formData.append("interInstitution", user.interInstitution);
        formData.append("interSpecialization", user.interSpecialization);
        formData.append("interCourseType", user.interCourseType);
        formData.append("interBoard", user.interBoard);
        formData.append("interTotalMarks", user.interTotalMarks);
        formData.append("interObtainMarks", user.interObtainMarks);
        formData.append("interPercentage", user.interPercentage);
        formData.append("interCertificateDoc", user.interCertificateDoc);
        formData.append("englishTyping", user.englishTyping);
        formData.append("hindiTyping", user.hindiTyping);
        formData.append("additionalCertificateDoc", user.additionalCertificateDoc);
        formData.append("ugCourse", user.ugCourse);
        formData.append("ugPassingYear", user.ugPassingYear);
        formData.append("ugInstitution", user.ugInstitution);
        formData.append("ugUniversity", user.ugUniversity);
        formData.append("ugSpecialization", user.ugSpecialization);
        formData.append("ugTotalMarks", user.ugTotalMarks);
        formData.append("ugObtainMarks", user.ugObtainMarks);
        formData.append("ugPercentage", user.ugPercentage);
        formData.append("ugCourseType", user.ugCourseType);
        formData.append("ugCertificateDoc", user.ugCertificateDoc);
        formData.append("pgCourse", user.pgCourse);
        formData.append("pgPassingYear", user.pgPassingYear);
        formData.append("pgInstitution", user.pgInstitution);
        formData.append("pgUniversity", user.pgUniversity);
        formData.append("pgSpecialization", user.pgSpecialization);
        formData.append("pgTotalMarks", user.pgTotalMarks);
        formData.append("pgObtainMarks", user.pgObtainMarks);
        formData.append("pgPercentage", user.pgPercentage);
        formData.append("pgCourseType", user.pgCourseType);
        formData.append("pgCertificateDoc", user.pgCertificateDoc);
        formData.append("diplomaYear", user.diplomaYear);
        formData.append("diplomaInstitution", user.diplomaInstitution);
        formData.append("diplomaSpecialization", user.diplomaSpecialization);
        formData.append("diplomaBranch", user.diplomaBranch);
        formData.append("diplomaUniversity", user.diplomaUniversity);
        formData.append("diplomaTotalMarks", user.diplomaTotalMarks);
        formData.append("diplomaObtainMarks", user.diplomaObtainMarks);
        formData.append("diplomaPercentage", user.diplomaPercentage);
        formData.append("diplomaCourseType", user.diplomaCourseType);
        formData.append("diplomaCertificateDoc", user.diplomaCertificateDoc);
        formData.append("degreeYear", user.degreeYear);
        formData.append("degreeInstitution", user.degreeInstitution);
        formData.append("degreeSpecialization", user.degreeSpecialization);
        formData.append("degreeCourseType", user.degreeCourseType);
        formData.append("degreeBranch", user.degreeBranch);
        formData.append("degreeUniversity", user.degreeUniversity);
        formData.append("degreeTotalMarks", user.degreeTotalMarks);
        formData.append("degreeMarksObtain", user.degreeMarksObtain);
        formData.append("degreePercentage", user.degreePercentage);
        formData.append("degreeCertificateDoc", user.degreeCertificateDoc);
        formData.append("companyName", user.companyName);

        formData.append("employmentFrom", (new Date(user.employmentFrom)).toDateString());
        formData.append("employmentTo", (new Date(user.employmentTo)).toDateString());
        formData.append("cinNumber", user.cinNumber);
        formData.append("workNature", user.workNature);
        formData.append("monthlySalary", user.monthlySalary);
        formData.append("temporaryPermanent", user.temporaryPermanent);
        formData.append("experienceCertificateDoc", user.experienceCertificateDoc);
        formData.append("licenceNumberLmv", user.licenceNumberLmv);
        formData.append("issueDateLmv", (new Date(user.issueDateLmv)).toDateString());
        // formData.append("issueDateLmv", user.issueDateLmv);
        formData.append("validUptoLmv", (new Date(user.validUptoLmv)).toDateString());
        // formData.append("validUptoLmv", user.validUptoLmv);
        formData.append("issuingAuthorityLmv", user.issuingAuthorityLmv);
        formData.append("licenceNumberHgmv", user.licenceNumberHgmv);
        formData.append("issueDateHgmv", (new Date(user.issueDateHgmv)).toDateString());
        formData.append("validUptoHgmv", (new Date(user.validUptoHgmv)).toDateString());
        formData.append("issuingAuthorityHlmv", user.issuingAuthorityHlmv);
        formData.append("drivingLicenseDoc", user.drivingLicenseDoc);
        formData.append("drivingExperienceDoc", user.drivingExperienceDoc);
        formData.append("browsePhotoDoc", user.browsePhotoDoc);
        formData.append("browseSignatureDoc", user.browseSignatureDoc);

        return this.http.post('http://localhost:8080/api/register/candidate/register', formData, options);
        //     return this.http.post('/api/candidate/register', user);
    }


    uploadDocument(docName, jobId) {
        let headers = new Headers();
        headers.append('enctype', 'multipart/form-data');
        let options = new RequestOptions({ headers: headers });

        let formData = new FormData();
        formData.append("postDetailsDocument", docName);
        formData.append("jobId", jobId);
        return this.http.post('http://localhost:8080/api/admin/upload/job/document', formData, options);
    }


}