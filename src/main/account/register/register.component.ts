import { Component, OnInit, AfterViewInit, Renderer, ElementRef } from '@angular/core';
// import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { Register } from './register.service';
// import { LoginModalService } from '../../shared';

@Component({
    // selector: 'jhi-register',
    templateUrl: './register.html',
    providers: [Register]
})
export class RegisterComponent implements OnInit, AfterViewInit {

    confirmPassword: string;
    doNotMatch: string;
    error: string;
    errorEmailExists: string;
    errorUserExists: string;
    registerAccount: any;
    success: boolean;
    // modalRef: NgbModalRef;


    public qualificationDetails: boolean = false;
    public uploadDocument: boolean = false;
    public finalSubmit: boolean = false;
    public personalDetails: boolean = true;
    public activePersonal: boolean = true;
    public activeQualification: boolean = false;
    public activeDocument: boolean = false;
    public activeFinal: boolean = false;
    public adharCheck: boolean = true;
    public communityCheck: boolean = true;
    public sameAddressCheck: boolean = false;
    public samePostalAddress: boolean = false;
    public exServiceCheck: boolean = true;
    public nccBCheck: boolean = true;
    public nccCCheck: boolean = true;
    public sportsCheck: boolean = true;
    public degreeCheck: boolean = true;
    public broGREFCheck: boolean = true;
    public sonGREFCheck: boolean = true;

    public highFileSize: boolean = false;
    public highFileType: boolean = false;
    public interFileSize: boolean = false;
    public interFileType: boolean = false;
    public gradFileSize: boolean = false;
    public gradFileType: boolean = false;
    public postGradFileType: boolean = false;
    public postGradFileSize: boolean = false;
    public techFileSize: boolean = false;
    public techFileType: boolean = false;

    public photoSize: boolean = false;
    public photoType: boolean = false;
    public signSize: boolean = false;
    public signType: boolean = false;
    public expFileSize: boolean = false;
    public expFileType: boolean = false;

    public nccbFileSize: boolean = false;
    public nccbFileType: boolean = false;
    public ncccFileSize: boolean = false;
    public ncccFileType: boolean = false;
    public sportsFileType: boolean = false;
    public sportsFileSize: boolean = false;


    public user: any = {};

    constructor(
        // private loginModalService: LoginModalService,
        private registerService: Register,
        private elementRef: ElementRef,
        private renderer: Renderer
    ) {
        this.user.category = 0;
        this.user.communityName = 0;
        this.user.nation = 0;
        this.user.maritalStatus = 0;
        this.user.permanentState = 0;
        this.user.permanentCity = 0;
        this.user.postalState = 0;
        this.user.postalCity = 0;
    }

    ngOnInit() {
        this.success = false;
        this.registerAccount = {};
    }

    ngAfterViewInit() {
        // this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#login'), 'focus', []);
    }

    register() {
        if (this.registerAccount.password !== this.confirmPassword) {
            this.doNotMatch = 'ERROR';
        } else {
            this.doNotMatch = null;
            this.error = null;
            this.errorUserExists = null;
            this.errorEmailExists = null;
            this.registerAccount.langKey = 'en';
            this.registerService.save(this.registerAccount).subscribe(() => {
                this.success = true;
            }, (response) => this.processError(response));
        }
    }

    private processError(response) {
        this.success = null;
        if (response.status === 400 && response._body === 'login already in use') {
            this.errorUserExists = 'ERROR';
        } else if (response.status === 400 && response._body === 'email address already in use') {
            this.errorEmailExists = 'ERROR';
        } else {
            this.error = 'ERROR';
        }
    }


      personalDetail() {
        this.qualificationDetails = false;
        this.uploadDocument = false;
        this.finalSubmit = false;
        this.personalDetails = true;
    }

    addQualification() {
        if (this.user.category == 0 || this.user.nation == 0 || this.user.permanentState == 0 || this.user.permanentCity == 0) {
            return;
        }
        if (!this.samePostalAddress && (this.user.postalState == 0 || this.user.postalCity == 0)) {

            return;
        }
        if (!this.communityCheck && this.user.communityName == 0) {
            return;
        }
        this.qualificationDetails = true;
        this.uploadDocument = false;
        this.finalSubmit = false;
        this.personalDetails = false;

        this.activeQualification = true;

    }


    nccbfileChange(event) {
        this.nccbFileSize = false;
        this.nccbFileType = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.nccBCertificateDoc = file;
                }
                else {
                    return this.nccbFileType = true;
                }
            }
            else {
                return this.nccbFileSize = true;
            }
        }
    }

    ncccfileChange(event) {
        this.ncccFileType = false;
        this.ncccFileSize = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.nccCCertificateDoc = file;
                }
                else {
                    return this.ncccFileType = true;
                }
            }
            else {
                return this.ncccFileSize = true;
            }
        }
    }

    sportsfileChange(event) {
        this.sportsFileType = false;
        this.sportsFileSize = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.sportsManDoc = file;
                }
                else {
                    return this.sportsFileType = true;
                }
            }
            else {
                return this.sportsFileSize = true;
            }
        }
    }

    highfileChange(event) {
        this.highFileType = false;
        this.highFileSize = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.highschoolDoc = file;
                }
                else {
                    return this.highFileType = true;
                }
            }
            else {
                return this.highFileSize = true;
            }
        }
    }


    interfileChange(event) {
        this.interFileType = false;
        this.interFileSize = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.interDoc = file;
                }
                else {
                    return this.interFileType = true;
                }
            }
            else {
                return this.interFileSize = true;
            }
        }
    }

    gradfileChange(event) {
        this.gradFileType = false;
        this.gradFileSize = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.graduationDoc = file;
                }
                else {
                    return this.gradFileType = true;
                }
            }
            else {
                return this.gradFileSize = true;
            }
        }
    }

    postGradfileChange(event) {
        this.postGradFileType = false;
        this.postGradFileSize = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.postGraduationDoc = file;
                }
                else {
                    return this.postGradFileType = true;
                }
            }
            else {
                return this.postGradFileSize = true;
            }
        }
    }

    techfileChange(event) {
        this.techFileType = false;
        this.techFileSize = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.techGradDoc = file;
                }
                else {
                    return this.techFileType = true;
                }
            }
            else {
                return this.techFileSize = true;
            }
        }
    }


    signChange(event) {
        this.signType = false;
        this.signSize = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.signature = file;
                }
                else {
                    return this.signType = true;
                }
            }
            else {
                return this.signSize = true;
            }
        }
    }

    photoChange(event) {
        this.photoType = false;
        this.photoSize = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.photo = file;
                }
                else {
                    return this.photoType = true;
                }
            }
            else {
                return this.photoSize = true;
            }
        }
    }



    experienceChange(event) {
        this.expFileType = false;
        this.expFileType = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.experienceDoc = file;
                }
                else {
                    return this.expFileType = true;
                }
            }
            else {
                return this.expFileType = true;
            }
        }
    }

    addDocument() {

        this.qualificationDetails = false;
        this.uploadDocument = true;
        this.finalSubmit = false;
        this.personalDetails = false;

        this.activeDocument = true;
    }

    preview() {
        this.qualificationDetails = false;
        this.uploadDocument = false;
        this.finalSubmit = true;
        this.personalDetails = false;
        this.activeFinal = true;
    }

    qualificationBack() {
        this.personalDetails = true;
        this.qualificationDetails = false;
        this.uploadDocument = false;
        this.finalSubmit = false;
    }


    uploadDocBack() {
        this.personalDetails = false;
        this.qualificationDetails = true;
        this.uploadDocument = false;
        this.finalSubmit = false;
    }

    adharChange(value: boolean) {
        if (value) {
            this.adharCheck = false;
        } else {
            this.adharCheck = true;
            this.user.adharCardNumber = '';
        }
    }

    communityChange(value: boolean) {
        if (value) {
            this.communityCheck = false;
        } else {
            this.communityCheck = true;
            this.user.communityName = 0;
        }
    }

    addressChange() {
        if (this.sameAddressCheck) {
            this.samePostalAddress = true;
        }
        else {
            this.samePostalAddress = false;
        }
    }

    exServiceChange(value: boolean) {
        if (value) {
            this.exServiceCheck = false;
        } else {
            this.exServiceCheck = true;
            this.user.armyNumber = '';
            this.user.armyRank = '';
        }
    }

    sonGREFChange(value: boolean) {
        if (value) {
            this.sonGREFCheck = false;
        } else {
            this.sonGREFCheck = true;
            this.user.sonGSNumber = '';
            this.user.sonRank = '';
            this.user.sonName = '';
        }
    }

    broGREFChange(value: boolean) {
        if (value) {
            this.broGREFCheck = false;
        } else {
            this.broGREFCheck = true;
            this.user.brotherArmy = '';
            this.user.brotherRank = '';
            this.user.brotherName = '';
        }
    }

    nccBChange(value: boolean) {
        if (value) {
            this.nccBCheck = false;
        } else {
            this.nccBCheck = true;
        }
    }

    nccCChange(value: boolean) {
        if (value) {
            this.nccCCheck = false;
        } else {
            this.nccCCheck = true;
        }
    }


    sportsChange(value: boolean) {
        if (value) {
            this.sportsCheck = false;
        } else {
            this.sportsCheck = true;
        }
    }

    degreeChange(value: boolean) {
        if (value) {
            this.degreeCheck = false;
        } else {
            this.degreeCheck = true;
        }
}
}