import { Component, ViewChild } from '@angular/core';
// import { GetService } from '../Services/get.service';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
    selector: 'result',
    templateUrl: './result.html',
    providers: []
})


export class ResultComponent {

    public result: any = {};

    
    minDateDob = new Date(1992, 5, 10);
    maxDateDob = new Date();
    
    bsConfig: Partial<BsDatepickerConfig>;
    colorTheme = "theme-red";

    constructor(private fb: FormBuilder) {
        this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
        // this.resultForm = fb.group({
        //     nameControl: new FormControl('', Validators.required),
        //     dobControl: new FormControl('', Validators.required),
        //     fatherNameControl: new FormControl('', Validators.required),
        // });
        

    }


    serachResult() {
    }
}