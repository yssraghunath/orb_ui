import { Component, ViewChild, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../Services/authentication.service';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
    selector: 'reset',
    templateUrl: './reset.html',
    providers: []
})


export class ResetComponent implements OnInit {
    public reset: any = {};
    public error: boolean = false;
    public pageSuccess: boolean = false;
    public errorEmailNotExists: string;
    public success: string;
    
    minDateDob = new Date(1992, 5, 10);
    maxDateDob = new Date();
    
    bsConfig: Partial<BsDatepickerConfig>;
    colorTheme = "theme-red";

    constructor(private authenticationService: AuthenticationService) {
      

    }
    ngOnInit() {
        this.reset = {};
    }

    requestReset() {
        this.error = false;
        this.errorEmailNotExists = null;

        this.authenticationService.save(this.reset.email).subscribe(() => {
            this.success = 'OK';
            this.pageSuccess = true;
        }, (response) => {
            this.success = null;
            if (response.status === 400 && response.data === 'email address not registered') {
                this.errorEmailNotExists = 'ERROR';
            } else {
                this.error = true;
                this.pageSuccess = false;
            }
        });
    }
}