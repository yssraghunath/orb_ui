import { Component, ViewChild, OnInit } from '@angular/core';
import { AuthenticationService, SharedService, GetService } from '../../../Services/index';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    templateUrl: './preview.html',
    providers: []
})


export class PreviewComponent {

    private registerId: any;
    public personals: any;

    constructor(private authenticationService: AuthenticationService,
        private sharedService: SharedService,
        private getService: GetService, private router: Router) {
        this.registerId = this.sharedService.getRegisterId();
        if (this.registerId == undefined) {
            this.router.navigate(['/candidate/login']);
        }

    }

    ngOnInit() {
        this.getPersonalDetails();
    }

    getPersonalDetails() {
        // let value = "3631721324";
        this.getService.getPersonalDetails(this.registerId).subscribe(
            data => {
                console.log("getPersonalDetails :", data);
                if (!data.payment) {
                    // this.router.navigate(['/candidate/payment']);
                }
                this.personals = data;
            },
            error => {
                // this.alertService.error(error);
                // this.loading = false;
            });
    }

    myprint(){
        window.print();
    }

}