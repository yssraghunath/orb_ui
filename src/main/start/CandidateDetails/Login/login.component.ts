import { Component, ViewChild, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService, AlertService, LoginService, GetService, SharedService } from '../../../Services/index';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { JhiEventManager } from 'ng-jhipster';
import { StateStorageService } from '../../../shared/auth/index';
// import { ModalComponent } from '../../../Modal/modal.component'; //Changes By danish

@Component({
    selector: 'login',
    templateUrl: './login.html',
    // providers: [AuthenticationService, AlertService, GetService]
})


export class LoginComponent {
    // @ViewChild(ModalComponent) popUp: ModalComponent; //Changes By Danish
    public returnUrl: string;
    public candidate: any = {};
    public authenticationError: boolean;
    public loading = false;

    constructor(private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private sharedService:SharedService,
        public authenticationService: AuthenticationService,
        public alertService: AlertService,
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private stateStorageService: StateStorageService,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private getServie: GetService
    ) {
    }

    ngAfterViewInit() {
        // this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', []);
    }

    public resolved(captchaResponse: string) {
        console.log(`Resolved captcha with response ${captchaResponse}:`);
    }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    }
    login() {
        this.loading = true;
        // let value= '121212';
        // this.getServie.getData(value)
        //     .subscribe(
        //     data => {
        //         this.router.navigate([this.returnUrl]);
        //     },
        //     error => {
        //         this.alertService.error(error);
        //     });

        this.loginService.login({
            username: this.candidate.username,
            password: this.candidate.password,
            rememberMe: this.candidate.rememberMe
        }).then(() => {
            this.loading = false;
            this.authenticationError = false;
            // this.popUp.getError("login First") //Changes By Danish
            const redirect = this.stateStorageService.getUrl();
            if (redirect) {
                this.stateStorageService.storeUrl(null);
                this.router.navigate([redirect]);
            }
            this.sharedService.saveRegisterData(this.candidate.username);
            this.router.navigate(['/candidate/login/preview']);
        }).catch(() => {
            this.loading = false;
            this.authenticationError = true;
        });
    }

    register() {
        this.router.navigate(['/register']);
    }

    requestResetPassword() {
        this.router.navigate(['/reset', 'request']);
    }
}