import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { DecimalPipe, DatePipe } from '@angular/common';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ModalComponent } from '../../../Modal/modal.component';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { AuthenticationService, AlertService, LoginService, GetService, SharedService } from '../../../Services/index';

@Component({
    templateUrl: './payment.html',
})


export class PaymentComponent {


    public onlinefeeCheck: boolean;
    public offlinefeeCheck: boolean;
    public debitCard: boolean;
    public paymentDetails: boolean;
    public creditCrad: boolean;
    public bhimUpi: boolean;
    public netBanking: boolean;
   
     public activedebitCard:boolean       

    public user: any = {
    };

    constructor() {



    }

 paymentDetail() {
        this.debitCard = true;
        this.paymentDetails = true;
        this.creditCrad = false;
        this.bhimUpi = false;
        this.netBanking = false;
        
        this.activedebitCard=true;

    }

    feeChange(value: boolean) {
        if (value) {
            this.onlinefeeCheck = true;
            this.offlinefeeCheck = false;
        } else {
            this.offlinefeeCheck = true;
            this.onlinefeeCheck = false;

        }
    }


}