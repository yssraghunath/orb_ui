import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { DecimalPipe, DatePipe } from '@angular/common';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ModalComponent } from '../../../Modal/modal.component';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { AuthenticationService, AlertService, LoginService, GetService, SharedService } from '../../../Services/index';
import { debuglog } from 'util';


@Component({
    templateUrl: './personal.html',
    providers: [ModalComponent]
})


export class PersonalComponent {
    @ViewChild(ModalComponent) popUp: ModalComponent;
    public essentialDetails: boolean = false;
    // public uploadDocument: boolean = false;
    public personalDetails: boolean = true;
    public activePersonal: boolean = true;
    public activeEssential: boolean = false;
    public activeGraduation: boolean = false;
    public activeTechnical: boolean = false;
    public activeExperience: boolean = false;
    public activeDriving: boolean = false;
    public activeUpload: boolean = false;
    public activePreview: boolean = false;
    public activePayment: boolean = false;

    public adharCheck: boolean = true;
    public disableCheck: boolean = true;
    public communityCheck: boolean = true;
    public sameAddressCheck: boolean = false;
    public samePostalAddress: boolean = false;
    public exServiceCheck: boolean = true;

    public centralCheck: boolean = true;

    public nccBCheck: boolean = true;
    public nccCCheck: boolean = true;
    public sportsCheck: boolean = true;
    public degreeCheck: boolean = true;
    public empExBroCheck: boolean = true;
    public broGREFCheck: boolean = true;
    public sonGREFCheck: boolean = true;
    public highGradeCheck: boolean = true;
    public interGradeCheck: boolean = true;
    public highFileSize: boolean = false;
    public highFileType: boolean = false;
    public interFileSize: boolean = false;
    public interFileType: boolean = false;
    public gradFileSize: boolean = false;
    public gradFileType: boolean = false;
    public postGradFileType: boolean = false;
    public postGradFileSize: boolean = false;
    public techFileSize: boolean = false;
    public techFileType: boolean = false;

    public photoSize: boolean = false;
    public photoType: boolean = false;
    public signSize: boolean = false;
    public signType: boolean = false;
    public expFileSize: boolean = false;
    public expFileType: boolean = false;

    public drivingFileType: boolean = false;
    public drivingFileSize: boolean = false;
    public licenseFileType: boolean = false;
    public licenseFileSize: boolean = false;

    public categoryFileSize: boolean = false;
    public categoryFileType: boolean = false;
    public nationFileSize: boolean = false;
    public nationFileType: boolean = false;
    public aadhaarFileSize: boolean = false;
    public aadhaarFileType: boolean = false;
    public centralFileSize: boolean = false;
    public centralFileType: boolean = false;
    public nccbFileSize: boolean = false;
    public nccbFileType: boolean = false;
    public ncccFileSize: boolean = false;
    public ncccFileType: boolean = false;
    public sportsFileType: boolean = false;
    public sportsFileSize: boolean = false;
    public addFileType: boolean = false;
    public addFileSize: boolean = false;

    public diplomaFileType: boolean = false;
    public diplomaFileSize: boolean = false;
    public error: string;
    public errorEmailExists: string;
    public errorUserExists: string;
    public success: boolean;
    public previewButton: boolean = true;

    public graduationDetails: boolean = false;
    public technicalDetails: boolean = false;
    public experienceDetails: boolean = false;
    public drivingDetails: boolean = false;
    public paymentDetails: boolean = false;
    public basicDetails: boolean = true;
    public finalSubmit: boolean = false;
    public uploadDetails: boolean = false;
    public nationRequired: boolean = false;
    public categoryRequired: boolean = false;
    public aadhaarfileRequired: boolean = false;
    public centralfileRequired: boolean = false;
    public nccbfileRequired: boolean = false;
    public nccfileRequired: boolean = false;
    public sportsfileRequired: boolean = false;
    public highRequired: boolean = false;
    public interRequired: boolean = false;
    public additionalRequired: boolean = false;
    public gradRequired: boolean = false;
    public postGradRequired: boolean = false;
    public degreeRequired: boolean = false;
    public diplomaRequired: boolean = false;
    public photoRequired: boolean = false;
    public signRequired: boolean = false;
    public experienceRequired: boolean = false;
    public drivingRequired: boolean = false;
    public licenseRequired: boolean = false;
    public highObtainGreater: boolean = false;
    public interObtainGreater: boolean = false;
    public ugObtainGreater: boolean = false;
    public pgObtainGreater: boolean = false;
    public diplomaObtainGreater: boolean = false;
    public degreeObtainGreater: boolean = false;
    public emailExists: boolean = false;
    public createSuccess: boolean = false;
    public loading = false;
    public categoryCer: boolean;
    public nationcheck: boolean;
    public ageInvalid: boolean;
    public ageMessage: string;

    public user: any = {};
    public states: any = [];
    public cities: any = [];
    public postalcities: any = [];
    public boards: any = [];
    public universities: any = [];
    public gradcourses: any = [];
    public postcourses: any = [];
    public dipcourses: any = [];
    public degcourses: any = [];
    public personals: any = [];
    public jobs: any;
    public formFields: any = {};
    public amountFields: any = {};
    public categories: any = [];
    public years: any = [];
    public relaxAge: any; // admin given relaxation
    public appliedAge: any;// candidate age -relaxation age
    public jobmaxAge: any; //apply max age by admin
    public jobminAge: any; // apply min age by admin
    public highMessage: string;
    public interMessage: string;
    public ugMessage: string;
    public pgMessage: string;
    public highPercentFlag: boolean;
    public interPercentFlag: boolean;
    public pgPercentFlag: boolean;
    public ugPercentFlag: boolean;
    public diplomaMessage: string;
    public degreeMessage: string;
    public degreePercentFlag: boolean;
    public diplomaPercentFlag: boolean;




    minDate: Date;
    maxDate: Date;

    minDateservedfrom = new Date(1992, 5, 10);
    maxDateservedfrom = new Date();
    minDateservedto = new Date(1992, 5, 10);
    maxDateservedto = new Date();

    minDateExempfrom = new Date(1992, 5, 10);
    maxDateExempfrom = new Date();
    minDateExempto = new Date(1992, 5, 10);
    maxsDateExempto = new Date();


    minDateExpFrom = new Date(1992, 5, 10);
    maxDateExpFrom = new Date();
    minDateExpTO = new Date(1992, 5, 10);
    maxDateExpTo = new Date();

    minDateDlmvFrom = new Date(1992, 5, 10);
    maxDateDlmvFrom = new Date();
    minDateDlmvTO = new Date();
    maxDateDlmvTo = new Date(2100, 5, 12);

    minDateDhgmvFrom = new Date(1992, 5, 10);
    maxDateDhgmvFrom = new Date();
    minDateDhgmvTO = new Date();
    maxDateDhgmvTo = new Date(2100, 5, 12);

    public invalidDob: boolean;

    public invalidPercentage: boolean;

    bsConfig: Partial<BsDatepickerConfig>;
    colorTheme = "theme-red";
    public calyear = {};



    constructor(public authenticationService: AuthenticationService,
        private getService: GetService, private alertService: AlertService,
        private decimalPipe: DecimalPipe, private route: ActivatedRoute,
        public sharedService: SharedService, private router: Router,
        private datePipe: DatePipe, public smModal: ModalComponent) {
        this.user.category = 0;
        this.user.communityName = 0;
        this.user.nationality = 0;
        this.user.maritalStatus = 0;
        this.user.permanentState = 0;
        this.user.permanentCity = 0;
        this.user.postalState = 0;
        this.user.postalCity = 0;
        this.user.gender = 0;
        this.user.highYear = 0;
        this.user.highBoard = 0;
        this.user.highCourseType = 0;
        this.user.interYear = 0;
        this.user.interCourseType = 0;
        this.user.interBoard = 0;
        this.user.ugCourse = 0;
        this.user.ugPassingYear = 0;
        this.user.ugUniversity = 0;
        this.user.ugCourseType = 0;
        this.user.pgCourse = 0;
        this.user.pgPassingYear = 0;
        this.user.pgUniversity = 0;
        this.user.pgCourseType = 0;
        this.user.diplomaBranch = 0;
        this.user.diplomaYear = 0;
        this.user.diplomaUniversity = 0;
        this.user.diplomaCourseType = 0;
        this.user.degreeBranch = 0;
        this.user.degreeUniversity = 0;
        this.user.degreeYear = 0;
        this.user.degreeCourseType = 0;
        this.sharedService = sharedService;
        this.jobs = sharedService.getData();


        // this.minDate = new Date();
        // this.maxDate = new Date();
        // this.minDate.setDate(this.minDate.getDate() - 1);
        // this.maxDate.setDate(this.maxDate.getDate() + 7);
        this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    }

    setDateOfBirth(cat) {
        // console.log(cat.target.value);
        // if (cat.target.value == "SC") {
        //     this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
        //     this.maxDate = new Date(this.maxDate.getFullYear(), 5, 10);
        //     console.log(this.maxDate);
        //     this.minDate = new Date();
        // }
    }

    ngOnInit() {
        if (this.jobs == undefined) {
            this.popUp.getError("Please select job first");
        }
        this.getState();
        this.getUniversity();
        this.getBoard();
        this.getCourse();
        this.getPostCourse();
        this.getDiplomaCourse();
        this.getDegreeCourse();
        this.getCategory();
        this.getYears();
        this.getFormFields();
        this.createSuccess = false;
    }

    getFormFields() {
        if (this.jobs != undefined) {
            this.jobs.jobId = this.jobs.id;
        }
        this.getService.getFormFields(this.jobs.jobId).subscribe(
            data => {
                this.formFields = data;
                console.log("Form fields :", data);

                this.getService.getAmountFields(this.formFields.id).subscribe(
                    data => {
                        this.amountFields = data;

                        console.log("Amount fields :", data);
                        // this.router.navigate(['/login']);
                    },
                    error => {
                        this.alertService.error(error);
                        // this.loading = false;
                    });
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });
    }

    getState() {
        this.getService.getState().subscribe(
            data => {
                // console.log("States :", data);
                // this.activeJobs = data;
                this.states = data;
                // this.router.navigate(['/login']);
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });
    }

    getCity(value: any) {
        this.getService.getCity(value).subscribe(
            data => {
                // console.log("cities :", data);
                // this.activeJobs = data;
                this.cities = data;
                this.user.permanentCity = data[0].name;
                // this.router.navigate(['/login']);
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });
    }

    getPostalCity(value: any) {
        this.getService.getCity(value).subscribe(
            data => {
                // console.log("postal cities :", data);
                this.postalcities = data;
            },
            error => {
                this.alertService.error(error);
            });
    }

    getBoard() {
        this.getService.getBoard().subscribe(
            data => {
                this.boards = data;
            },
            error => {
                // this.loading = false;
            });
    }

    getUniversity() {
        this.getService.getUniversity().subscribe(
            data => {
                this.universities = data;
                this.alertService.success('Registration successful', true);
                // this.router.navigate(['/login']);
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });
    }

    getCourse() {
        let value = "Graduation";
        this.getService.getCourse(value).subscribe(
            data => {
                // console.log("Course :", data);
                this.gradcourses = data;
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });
    }

    checkCategory() {
        if (this.user.category.length > 6) {
            return this.categoryCer = true;
        } else {
            return this.categoryCer = false;
        }
    }

    checkNation() {
        if (this.user.nationality.length > 7) {
            return this.nationcheck = false;
        } else {
            return this.nationcheck = true;
        }
    }

    getPostCourse() {
        let value = "Post_Graduation";
        this.getService.getCourse(value).subscribe(
            data => {
                // console.log("post Course :", data);
                this.postcourses = data;
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });

    }

    getDiplomaCourse() {
        let value = "Diploma";
        this.getService.getCourse(value).subscribe(
            data => {
                // console.log("Degree Course :", data);
                this.dipcourses = data;
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });

    }

    getDegreeCourse() {
        let value = "Degree";
        this.getService.getCourse(value).subscribe(
            data => {
                // console.log("Degree Course :", data);
                this.degcourses = data;
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });

    }

    getPersonalDetails() {
        this.loading = true;
        // let value = "3631721324";
        this.getService.getPersonalDetails(this.user.registrationId).subscribe(
            data => {
                console.log("getPersonalDetails :", data);
                this.personals = data;
                // this.loading = false;
            },
            error => {
                this.loading = false;
                this.alertService.error(error);
                // this.loading = false;
            });
    }

    getCategory() {
        // let value = "3631721324";
        this.getService.getCategory().subscribe(
            data => {
                this.categories = data;
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });
    }

    getYears() {
        // let value = "3631721324";
        this.getService.getYears().subscribe(
            data => {
                this.years = data;
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });
    }

    personalDetail() {


        this.basicDetails = true;
        this.personalDetails = true;
        this.essentialDetails = false;
        this.graduationDetails = false;
        this.technicalDetails = false;
        this.experienceDetails = false;
        this.drivingDetails = false;
        this.finalSubmit = false;
        this.uploadDetails = false;
        this.activePersonal = true;

    }

    essentialDetail() {

       

        // if (this.user.category == 0 || this.user.nationality == 0 || this.user.permanentState == 0 || this.user.permanentCity == 0) {
        //     return;
        // }
        // if (!this.samePostalAddress && (this.user.postalState == 0 || this.user.postalCity == 0)) {
        //     return;
        // }
        // if (!this.communityCheck && this.user.communityName == 0) {
        //     return;
        // }
        // if (this.emailExists) {
        //     return;
        // }
        // if (!this.categoryRequired) {
        //     return;
        // }
        // if (!this.aadhaarfileRequired) {
        //     return;
        // }
        // if (!this.centralfileRequired) {
        //     return;
        // }
        // if (!this.nccbfileRequired) {
        //     return;
        // }
        // if (!this.nccfileRequired) {
        //     return;
        // }
        // if (!this.sportsfileRequired) {
        //     return;
        // }
        if (!this.formFields.essentialQualificationForm) {
            this.graduationDetail();
        }
        else {

            this.ageInvalid = false;
            for (let i = 0; i < this.amountFields.length; i++) {
                // if (this.amountFields[i].categoryName == "UR") {
                //     if(categ == "General"){
                //     this.maxDate.setDate(this.amountFields[i].maxAgeDate);
                //     // this.maxDate = new Date(this.amountFields[i].maxAgeDate);
                //     this.minDate.setDate(this.amountFields[i].minAgeDate);
                //     // this.minDate = new Date(this.amountFields[i].minAgeDate);
                //     }
                // }

                // if (this.amountFields[i].categoryName == "SC") {
                //     if(categ == "SC"){
                //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
                //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
                //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
                //     this.minDate = new Date(this.amountFields[i].minAgeDate);
                //     }
                // }

                // if (this.amountFields[i].categoryName == "ST") {
                //     if(categ == "ST"){
                //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
                //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
                //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
                //     this.minDate = new Date(this.amountFields[i].minAgeDate);
                //     }
                // }

                // if (this.amountFields[i].categoryName == "OBC") {
                //     if(categ == "OBC"){
                //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
                //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
                //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
                //     this.minDate = new Date(this.amountFields[i].minAgeDate);
                //     }
                // }
                if (this.amountFields[i].categoryName == "SC") {

                    if (this.user.category == "SC") {
                        let minValue = this.amountFields[i].minAge;
                        let maxValue = this.amountFields[i].maxAge;

                        if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                            let relaxAge = this.amountFields[i].ageRelaxation;
                            let finalYear = this.calyear + relaxAge;

                            if ((finalYear < minValue || finalYear > maxValue)) {
                                this.ageMessage = "Your age is Invalid! sc1"
                                return this.ageInvalid = true;
                            }
                        } else {

                            if (this.calyear < minValue || this.calyear > maxValue) {

                                this.ageMessage = "Your age is Invalid! sc2"
                                return this.ageInvalid = true;
                            }
                        }
                    }
                }

                if (this.amountFields[i].categoryName == "ST") {
                    if (this.user.category == "ST") {
                        let minValue = this.amountFields[i].minAge;
                        let maxValue = this.amountFields[i].maxAge;

                        if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                            let relaxAge = this.amountFields[i].ageRelaxation;
                            let finalYear = this.calyear + relaxAge;

                            if (finalYear < minValue || finalYear > maxValue) {
                                this.ageMessage = "Your age is Invalid! st1"
                                return this.ageInvalid = true;
                            }
                        } else {

                            if (this.calyear < minValue || this.calyear > maxValue) {

                                this.ageMessage = "Your age is Invalid! st2"
                                return this.ageInvalid = true;
                            }
                        }
                    }
                }

                if (this.amountFields[i].categoryName == "OBC") {
                    if (this.user.category == "OBC") {
                        let minValue = this.amountFields[i].minAge;
                        let maxValue = this.amountFields[i].maxAge;

                        if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                            let relaxAge = this.amountFields[i].ageRelaxation;
                            let finalYear = this.calyear + relaxAge;

                            if (finalYear < minValue || finalYear > maxValue) {
                                this.ageMessage = "Your age is Invalid! obc1"
                                return this.ageInvalid = true;
                            }
                        } else {

                            if (this.calyear < minValue || this.calyear > maxValue) {

                                this.ageMessage = "Your age is Invalid! obc2"
                                return this.ageInvalid = true;
                            }
                        }
                    }
                }
                if (this.amountFields[i].categoryName == "UR") {
                    if (this.user.category == "General") {
                        let minValue = this.amountFields[i].minAge;
                        let maxValue = this.amountFields[i].maxAge;

                        if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
                            let relaxAge = this.amountFields[i].ageRelaxation;
                            let finalYear = this.calyear + relaxAge;

                            if (finalYear < minValue || finalYear > maxValue) {
                                this.ageMessage = "Your age is Invalid! ur1"
                                return this.ageInvalid = true;
                            }
                        } else {

                            if (this.calyear < minValue || this.calyear > maxValue) {

                                this.ageMessage = "Your age is Invalid! ur2"
                                return this.ageInvalid = true;
                            }
                        }
                    }
                }
            }

            // if (this.user.category == "SC") {
            //     if (this.user.dateOfBirth > this.formFields.jobDetails.expiryDate) {

            //         alert("Pleas Enter Valid Date of Birth");
            //         //this.message=("invalid")
            //         return this.invalidDob = true;
            //     }


            //     if (this.user.age > this.formFields.maxAge) {
            //         alert("Invalid Age");
            //         return this.invalidDob = true;
            //     }
            // }

            // if (this.user.category == "GEN") {
            //     if (this.user.dateOfBirth > this.formFields.jobDetails.expiryDate) {
            //         //alert("Pleas Enter Valid Date of Birth");
            //         return this.invalidDob = true;
            //     }

            //     if (this.user.age > this.formFields.maxAge) {
            //         alert("Invalid Age");
            //         return this.invalidDob = true;
            //     }
            // }

            // if (this.user.category == "ST") {
            //     if (this.user.dateOfBirth > this.formFields.expiryDate) {
            //         alert("Pleas Enter Valid Date of Birth");
            //         return this.invalidDob = true;
            //     }
            //     if (this.user.age > this.formFields.maxAge) {
            //         alert("Invalid Age");
            //         return this.invalidDob = true;
            //     }
            // }

            // if (this.user.category == "OBC") {
            //     if (this.user.dateOfBirth > this.formFields.jobDetails.expiryDate) {

            //         alert("Invalid Date of birth");
            //         return this.invalidDob = true;
            //     }

            //     if (this.user.age > this.formFields.maxAge) {
            //         alert("Invalid Age");
            //         return this.invalidDob = true;
            //     }
            // }


            // if (this.user.category == "General") {
            //     this.relaxAge = 0;
            //     if (this.user.dateOfBirth > this.formFields.expiryDate) {

            //         if(this.user.disablity == true){
            //                 this.relaxAge = this.relaxAge+2;
            //         }

            //         if(this.user.centralGovt == true) {
            //             this.relaxAge = this.relaxAge + (this.user.servedTo -this.user.servedFrom);   
            //         }

            //         if(this.user.empExGref == true) {
            //             this.relaxAge = this.relaxAge + (this.user.servedBroTo -this.user.servedBroFrom);   
            //         }

            //         this.appliedAge = this.user.age - this.relaxAge;
            //         if(this.appliedAge <=this.jobmaxAge && this.appliedAge >=this.jobminAge )
            //         {
            //             return this.invalidDob = false; 
            //         }
            //         else 
            //         return this.invalidDob = true;
            //     }
            // }


            // if (this.user.category == "SC") {
            //     this.relaxAge = 5;
            //     if (this.user.dateOfBirth > this.formFields.expiryDate) {

            //         if(this.user.disablity == true){
            //                 this.relaxAge = this.relaxAge+2;
            //         }

            //         if(this.user.centralGovt == true) {
            //             this.relaxAge = this.relaxAge + (this.user.servedTo -this.user.servedFrom);   
            //         }

            //         if(this.user.empExGref == true) {
            //             this.relaxAge = this.relaxAge + (this.user.servedBroTo -this.user.servedBroFrom);   
            //         }

            //         this.appliedAge = this.user.age - this.relaxAge;
            //         if(this.appliedAge <=this.jobmaxAge && this.appliedAge >=this.jobminAge )
            //         {
            //             return this.invalidDob = false; 
            //         }
            //         else 
            //         return this.invalidDob = true;
            //     }
            // }

            // if (this.user.category == "ST") {
            //     this.relaxAge = 5;
            //     if (this.user.dateOfBirth > this.formFields.expiryDate) {

            //         if(this.user.disablity == true){
            //                 this.relaxAge = this.relaxAge+2;
            //         }

            //         if(this.user.centralGovt == true) {
            //             this.relaxAge = this.relaxAge + (this.user.servedTo -this.user.servedFrom);   
            //         }

            //         if(this.user.empExGref == true) {
            //             this.relaxAge = this.relaxAge + (this.user.servedBroTo -this.user.servedBroFrom);   
            //         }

            //         this.appliedAge = this.user.age - this.relaxAge;
            //         if(this.appliedAge <=this.jobmaxAge && this.appliedAge >=this.jobminAge )
            //         {
            //             return this.invalidDob = false; 
            //         }
            //         else 
            //         return this.invalidDob = true;
            //     }
            // }

            // if (this.user.category == "OBC") {
            //         this.relaxAge = 2;
            //     if (this.user.dateOfBirth > this.formFields.expiryDate) {

            //         if(this.user.disablity == true){
            //                 this.relaxAge = this.relaxAge+2;
            //         }

            //         if(this.user.centralGovt == true) {
            //             this.relaxAge = this.relaxAge + (this.user.servedTo -this.user.servedFrom);   
            //         }

            //         if(this.user.empExGref == true) {
            //             this.relaxAge = this.relaxAge + (this.user.servedBroTo -this.user.servedBroFrom);   
            //         }

            //         this.appliedAge = this.user.age - this.relaxAge;
            //         if(this.appliedAge <=this.jobmaxAge && this.appliedAge >=this.jobminAge )
            //         {
            //             return this.invalidDob = false; 
            //         }
            //         else 
            //         return this.invalidDob = true;
            //     }
            // }


            if (this.samePostalAddress) {
                this.user.postalAddress = this.user.permanentAddress;
                this.user.postalState = this.user.permanentState;
                this.user.postalCity = this.user.permanentCity;
                this.user.postalPincode = this.user.permanentPincode;
            }


            this.personalDetails = false;
            this.essentialDetails = true;
            this.graduationDetails = false;
            this.technicalDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = false;
            this.finalSubmit = false;
            this.uploadDetails = false;
            this.activeEssential = true;
            this.paymentDetails = false;
        }
    }
    graduationDetail() {



        if (!this.formFields.otherQualificationForm) {
            this.technicalDetail();
        } else {
            if (!this.highRequired) {
                return;
            }
            if (!this.interRequired) {
                return;
            }
            if (!this.additionalRequired) {
                return;
            }
            // this.highPercentFlag = false;
            // this.interPercentFlag = false;
            // if (this.user.highPercentage < this.formFields.high.highMinPercent) {
            //     //alert("Invalid Percentage");
            //     this.highPercentFlag = true;
            //     this.highMessage = "Your are not fullfilling criteria";
            //     return this.highPercentFlag;

            // }

            // if (this.user.interPercentage < this.formFields.inter.interMinPercent) {
            //     this.interPercentFlag = true;
            //     this.interMessage = "Your are not fullfilling criteria";
            //     //alert("Invalid Percentage");
            //     return this.interPercentFlag;
            // }

            this.graduationDetails = true;
            this.essentialDetails = false;
            this.technicalDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = false;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.uploadDetails = false;
            this.activeGraduation = true;
            this.paymentDetails = false;
        }
    }
    chekPercentage() {


        this.highPercentFlag = false;
        this.interPercentFlag = false;
        if (this.user.highPercentage < this.formFields.high.highMinPercent) {
            //alert("Invalid Percentage");
            this.highPercentFlag = true;
            this.highMessage = "Your are not fullfilling criteria";
            return this.highPercentFlag;

        }

        if (this.user.interPercentage < this.formFields.inter.interMinPercent) {
            this.interPercentFlag = true;
            this.interMessage = "Your are not fullfilling criteria";
            //alert("Invalid Percentage");
            return this.interPercentFlag;
        }

        this.diplomaPercentFlag = false;
        this.degreePercentFlag = false;
        if (this.user.diplomaPercentage < this.formFields.diploma.diplomaMinPercent) {
            this.diplomaPercentFlag = true;
            //alert("Invalid Percentage");
            this.diplomaMessage = "Your are not fullfilling criteria";
            return this.diplomaPercentFlag = true;

        }

        if (this.user.degreePercentage < this.formFields.degree.degreeMinPercent) {
            this.degreePercentFlag = true;
            //alert("Invalid Percentage");
            this.degreeMessage = "Your are not fullfilling criteria";
            return this.degreePercentFlag = true;

        }

        this.ugPercentFlag = false;
        this.pgPercentFlag = false;
        if (this.user.ugPercentage < this.formFields.graduation.ugMinPercent) {
            this.ugPercentFlag = true;
            // alert("Invalid Percentage");
            this.ugMessage = "Your are not fullfilling criteria";
            return this.invalidPercentage = true;

        }

        if (this.user.pgPercentage < this.formFields.pgraduation.pgMinPercent) {
            //alert("Invalid Percentage");
            this.pgPercentFlag = true;

            this.pgMessage = "Your are not fullfilling criteria";
            return this.invalidPercentage = true;

        }
    }

    technicalDetail() {



        if (!this.formFields.technicalForm) {
            this.experienceDetail();
        } else {

            if (!this.gradRequired) {
                return;
            }
            if (!this.postGradRequired) {
                return;
            }

            // this.ugPercentFlag = false;
            // this.pgPercentFlag = false;
            // if (this.user.ugPercentage < this.formFields.graduation.ugMinPercent) {
            //     this.ugPercentFlag = true;
            //     // alert("Invalid Percentage");
            //     this.ugMessage = "Your are not fullfilling criteria";
            //     return this.invalidPercentage = true;

            // }

            // if (this.user.pgPercentage < this.formFields.pgraduation.pgMinPercent) {
            //     //alert("Invalid Percentage");
            //     this.pgPercentFlag = true;

            //     this.pgMessage = "Your are not fullfilling criteria";
            //     return this.invalidPercentage = true;

            // }

            this.technicalDetails = true;
            this.essentialDetails = false;
            this.graduationDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = false;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.uploadDetails = false;
            this.activeTechnical = true;
            this.paymentDetails = false;
        }
    }


    experienceDetail() {
        if (!this.formFields.experienceForm) {
            this.drivingDetail();
        } else {

            if (!this.diplomaRequired) {
                return;
            }
            if (!this.degreeRequired) {
                return;
            }

            // this.diplomaPercentFlag = false;
            // this.degreePercentFlag = false;
            // if (this.user.diplomaPercentage < this.formFields.diploma.diplomaMinPercent) {
            //     this.diplomaPercentFlag = true;
            //     //alert("Invalid Percentage");
            //     this.diplomaMessage = "Your are not fullfilling criteria";
            //     return this.diplomaPercentFlag = true;

            // }

            // if (this.user.degreePercentage < this.formFields.degree.degreeMinPercent) {
            //     this.degreePercentFlag = true;
            //     //alert("Invalid Percentage");
            //     this.degreeMessage = "Your are not fullfilling criteria";
            //     return this.degreePercentFlag = true;

            // }

            this.experienceDetails = true;
            this.essentialDetails = false;
            this.graduationDetails = false;
            this.technicalDetails = false;
            this.drivingDetails = false;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.uploadDetails = false;
            this.activeExperience = true;
            this.paymentDetails = false;
        }
    }

    drivingDetail() {
        if (!this.formFields.drivingLicenseForm) {
            this.addDocument();
        } else {
            if (!this.experienceRequired) {
                return;
            }


            this.uploadDetails = false;
            this.graduationDetails = false;
            this.essentialDetails = false;
            this.technicalDetails = false;
            this.experienceDetails = false;
            this.drivingDetails = true;
            this.personalDetails = false;
            this.finalSubmit = false;
            this.activeDriving = true;
            this.paymentDetails = false;
        }
    }

    addDocument() {

        this.uploadDetails = true;
        this.graduationDetails = false;
        this.essentialDetails = false;
        this.technicalDetails = false;
        this.experienceDetails = false;
        this.drivingDetails = false;
        this.personalDetails = false;
        this.basicDetails = false;
        this.finalSubmit = false;
        this.activeUpload = true;
        this.paymentDetails = false;
    }

    preview() {
        this.loading = true;
        this.uploadDetails = false;
        this.graduationDetails = false;
        this.essentialDetails = false;
        this.technicalDetails = false;
        this.experienceDetails = false;
        this.drivingDetails = false;
        this.personalDetails = false;
        this.finalSubmit = true;
        this.basicDetails = false;
        this.activePreview = true;
        this.paymentDetails = false;
        this.getPersonalDetails();


        // this.user.dateOfBirth = this.datePipe.transform(this.user.dateOfBirth, 'dd-MM-yyyy');
        // this.user.employmentFrom = this.datePipe.transform(this.user.employmentFrom, 'dd-MM-yyyy');
        // this.user.user.employmentTo = this.datePipe.transform(this.user.employmentTo, 'dd-MM-yyyy');
        // this.user.issueDateLmv = this.datePipe.transform(this.user.issueDateLmv, 'dd-MM-yyyy');
        // this.user.validUptoLmv = this.datePipe.transform(this.user.validUptoLmv, 'dd-MM-yyyy');
        // this.user.issueDateHgmv = this.datePipe.transform(this.user.issueDateHgmv, 'dd-MM-yyyy');
        // this.user.validUptoHgmvView = this.datePipe.transform(this.user.validUptoHgmvView, 'dd-MM-yyyy');

    }

    payment() {
        this.uploadDetails = false;
        this.graduationDetails = false;
        this.essentialDetails = false;
        this.technicalDetails = false;
        this.experienceDetails = false;
        this.drivingDetails = false;
        this.personalDetails = false;
        this.finalSubmit = false;
        this.basicDetails = false;
        this.activePayment = true;
        this.paymentDetails = true;

    }


    uploadImage() {
        this.previewButton = true;
        let now = new Date().getTime().toString().slice(7);
        if (this.user.mobileNumber > 0) {
            let mobile = (this.user.mobileNumber).slice(6)
            this.user.registrationId = now + mobile;
        }
        this.user.jobId = this.jobs.id;
        this.user.payment = false;
        this.user.finalSubmit = false;
        this.authenticationService.createCandidate(this.user).subscribe(data => {
            console.log("Saved data", data);
            this.previewButton = false;
            this.createSuccess = true;
        },
            error => {
                this.createSuccess = false;
                this.alertService.error(error);
            });

    }

    emailCheck(email: string) {
        this.getService.emailCheck(email).subscribe(
            data => {
                console.log("Email check :", data);
                // this.activeJobs = data;
                this.emailExists = data.present;
                this.alertService.success('Registration successful', true);
                // this.router.navigate(['/login']);
            },
            error => {
                this.alertService.error(error);
                // this.loading = false;
            });
    }
    // private processError(response) {
    //     this.success = null;
    //     if (response.status === 400 && response._body === 'login already in use') {
    //         this.errorUserExists = 'ERROR';
    //     } else if (response.status === 400 && response._body === 'email address already in use') {
    //         this.errorEmailExists = 'ERROR';
    //     } else {
    //         this.error = 'ERROR';
    //     }
    // }

    // setAge(categ) {

    //     this.ageInvalid = false;
    //     debugger
    //     for (let i = 0; i < this.amountFields.length; i++) {
    //         // if (this.amountFields[i].categoryName == "UR") {
    //         //     if(categ == "General"){
    //         //     this.maxDate.setDate(this.amountFields[i].maxAgeDate);
    //         //     // this.maxDate = new Date(this.amountFields[i].maxAgeDate);
    //         //     this.minDate.setDate(this.amountFields[i].minAgeDate);
    //         //     // this.minDate = new Date(this.amountFields[i].minAgeDate);
    //         //     }
    //         // }

    //         // if (this.amountFields[i].categoryName == "SC") {
    //         //     if(categ == "SC"){
    //         //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
    //         //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
    //         //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
    //         //     this.minDate = new Date(this.amountFields[i].minAgeDate);
    //         //     }
    //         // }

    //         // if (this.amountFields[i].categoryName == "ST") {
    //         //     if(categ == "ST"){
    //         //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
    //         //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
    //         //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
    //         //     this.minDate = new Date(this.amountFields[i].minAgeDate);
    //         //     }
    //         // }

    //         // if (this.amountFields[i].categoryName == "OBC") {
    //         //     if(categ == "OBC"){
    //         //     // this.maxDate.setDate(this.amountFields[i].maxAgeDate);
    //         //     this.maxDate = new Date(this.amountFields[i].maxAgeDate);
    //         //     // this.minDate.setDate(this.amountFields[i].minAgeDate);
    //         //     this.minDate = new Date(this.amountFields[i].minAgeDate);
    //         //     }
    //         // }
    //         if (this.amountFields[i].categoryName == "SC") {

    //             if (categ == "SC") {
    //                 let minValue = this.amountFields[i].minAge;
    //                 let maxValue = this.amountFields[i].maxAge;

    //                 if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
    //                     let relaxAge = this.amountFields[i].ageRelaxation;
    //                     let finalYear = this.calyear + relaxAge;

    //                     if ((finalYear < minValue || finalYear > maxValue)) {
    //                         this.ageMessage = "Your age is Invalid! sc1"
    //                         return this.ageInvalid = true;
    //                     }
    //                 } else {

    //                     if (this.calyear < minValue || this.calyear > maxValue) {

    //                         this.ageMessage = "Your age is Invalid! sc2"
    //                         return this.ageInvalid = true;
    //                     }
    //                 }
    //             }
    //         }

    //         if (this.amountFields[i].categoryName == "ST") {
    //             if (categ == "ST") {
    //                 let minValue = this.amountFields[i].minAge;
    //                 let maxValue = this.amountFields[i].maxAge;

    //                 if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
    //                     let relaxAge = this.amountFields[i].ageRelaxation;
    //                     let finalYear = this.calyear + relaxAge;

    //                     if (finalYear < minValue || finalYear > maxValue) {
    //                         this.ageMessage = "Your age is Invalid! st1"
    //                         return this.ageInvalid = true;
    //                     }
    //                 } else {

    //                     if (this.calyear < minValue || this.calyear > maxValue) {

    //                         this.ageMessage = "Your age is Invalid! st2"
    //                         return this.ageInvalid = true;
    //                     }
    //                 }
    //             }
    //         }

    //         if (this.amountFields[i].categoryName == "OBC") {
    //             if (categ == "OBC") {
    //                 let minValue = this.amountFields[i].minAge;
    //                 let maxValue = this.amountFields[i].maxAge;

    //                 if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
    //                     let relaxAge = this.amountFields[i].ageRelaxation;
    //                     let finalYear = this.calyear + relaxAge;

    //                     if (finalYear < minValue || finalYear > maxValue) {
    //                         this.ageMessage = "Your age is Invalid! obc1"
    //                         return this.ageInvalid = true;
    //                     }
    //                 } else {

    //                     if (this.calyear < minValue || this.calyear > maxValue) {

    //                         this.ageMessage = "Your age is Invalid! obc2"
    //                         return this.ageInvalid = true;
    //                     }
    //                 }
    //             }
    //         }
    //         if (this.amountFields[i].categoryName == "UR") {
    //             if (categ == "General") {
    //                 let minValue = this.amountFields[i].minAge;
    //                 let maxValue = this.amountFields[i].maxAge;

    //                 if (this.amountFields[i].ageRelaxation !== null && this.amountFields[i].ageRelaxation !== undefined) {
    //                     let relaxAge = this.amountFields[i].ageRelaxation;
    //                     let finalYear = this.calyear + relaxAge;

    //                     if (finalYear < minValue || finalYear > maxValue) {
    //                         this.ageMessage = "Your age is Invalid! ur1"
    //                         return this.ageInvalid = true;
    //                     }
    //                 } else {

    //                     if (this.calyear < minValue || this.calyear > maxValue) {

    //                         this.ageMessage = "Your age is Invalid! ur2"
    //                         return this.ageInvalid = true;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    getAge(value) {

        var now = new Date(); //Todays Date   
        // var birthday = dob;
        let birthday: any = this.datePipe.transform(value, 'yyyy-MM-dd');
        birthday = birthday.split("-");

        var dobYear = birthday[0];
        var dobMonth = birthday[1];
        var dobDay = birthday[2];

        var nowDay = now.getDate();
        var nowMonth = now.getMonth() + 1;  //jan=0 so month+1
        var nowYear = now.getFullYear();

        var ageyear = nowYear - dobYear;
        var agemonth = nowMonth - dobMonth;
        var ageday = nowDay - dobDay;
        if (agemonth < 0) {
            ageyear--;
            agemonth = (12 + agemonth);
        }
        if (nowDay < dobDay) {
            agemonth--;
            ageday = 30 + ageday;
        }

        this.user.age = ageyear + " years, " + agemonth + " months, " + ageday + " days";

        this.calyear = ageyear;

    }
    restrictNumeric(event) {
        return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }


    getHighPercentage() {
        this.highObtainGreater = false;
        if (this.user.highObtainMarks !== null && this.user.highTotalMarks !== null && this.user.highObtainMarks !== undefined && this.user.highTotalMarks !== undefined) {
            if (parseInt(this.user.highObtainMarks) > parseInt(this.user.highTotalMarks)) {
                return this.highObtainGreater = true;
            } else {
                this.user.highPercentage = (this.user.highObtainMarks / this.user.highTotalMarks) * 100;
            }

            this.user.highPercentage = this.decimalPipe.transform(this.user.highPercentage, '1.2-2');
        }
        // == is used to check for equality doesn't matter type and === matters for the type also.
    }

    getInterPercentage() {
        this.interObtainGreater = false;
        if (this.user.interObtainMarks !== null && this.user.interTotalMarks !== null && this.user.interObtainMarks !== undefined && this.user.interTotalMarks !== undefined) {
            if (parseInt(this.user.interObtainMarks) > parseInt(this.user.interTotalMarks)) {
                return this.interObtainGreater = true;
            } else {
                this.user.interPercentage = (this.user.interObtainMarks / this.user.interTotalMarks) * 100;
            }

            this.user.interPercentage = this.decimalPipe.transform(this.user.interPercentage, '1.2-2');
        }
        // == is used to check for equality doesn't matter type and === matters for the type also.
    }

    getUgPercentage() {
        this.ugObtainGreater = false;
        if (this.user.ugObtainMarks !== null && this.user.ugTotalMarks !== null && this.user.ugObtainMarks !== undefined && this.user.ugTotalMarks !== undefined) {
            if (parseInt(this.user.ugObtainMarks) > parseInt(this.user.ugTotalMarks)) {
                return this.ugObtainGreater = true;
            } else {
                this.user.ugPercentage = (this.user.ugObtainMarks / this.user.ugTotalMarks) * 100;
            }

            this.user.ugPercentage = this.decimalPipe.transform(this.user.ugPercentage, '1.2-2');
        }
    }

    getPgPercentage() {
        this.pgObtainGreater = false;
        if (this.user.pgObtainMarks !== null && this.user.pgTotalMarks !== null && this.user.pgObtainMarks !== undefined && this.user.pgTotalMarks !== undefined) {
            if (parseInt(this.user.pgObtainMarks) > parseInt(this.user.pgTotalMarks)) {
                return this.pgObtainGreater = true;
            } else {
                this.user.pgPercentage = (this.user.pgObtainMarks / this.user.pgTotalMarks) * 100;
            }

            this.user.pgPercentage = this.decimalPipe.transform(this.user.pgPercentage, '1.2-2');
        }
    }

    getDiplomaPercentage() {
        this.diplomaObtainGreater = false;
        if (this.user.diplomaObtainMarks !== null && this.user.diplomaTotalMarks !== null && this.user.diplomaObtainMarks !== undefined && this.user.diplomaTotalMarks !== undefined) {
            if (parseInt(this.user.diplomaObtainMarks) > parseInt(this.user.diplomaTotalMarks)) {
                return this.diplomaObtainGreater = true;
            } else {
                this.user.diplomaPercentage = (this.user.diplomaObtainMarks / this.user.diplomaTotalMarks) * 100;
            }

            this.user.diplomaPercentage = this.decimalPipe.transform(this.user.diplomaPercentage, '1.2-2');
        }
    }

    getDegreePercentage() {
        if (this.user.degreeMarksObtain !== null && this.user.degreeTotalMarks !== null && this.user.degreeMarksObtain !== undefined && this.user.degreeTotalMarks !== undefined) {
            if (parseInt(this.user.degreeMarksObtain) > parseInt(this.user.degreeTotalMarks)) {
                return this.degreeObtainGreater = true;
            } else {
                this.user.degreePercentage = (this.user.degreeMarksObtain / this.user.degreeTotalMarks) * 100;
            }

            this.user.degreePercentage = this.decimalPipe.transform(this.user.degreePercentage, '1.2-2');
        }
    }

    categoryfileChange(event) {
        this.categoryFileType = false;
        this.categoryFileSize = false;
        this.categoryRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.categoryRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.categoryCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.categoryCertificateDoc = file;
                }
                else {
                    return this.categoryFileType = true;
                }
            }
            else {
                return this.categoryFileSize = true;
            }
        }
    }

    nationfileChange(event) {
        this.nationFileType = false;
        this.nationFileSize = false;
        this.nationRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.nationRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.nationCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.nationCertificateDoc = file;
                }
                else {
                    return this.nationFileType = true;
                }
            }
            else {
                return this.nationFileSize = true;
            }
        }
    }

    aadhaarfileChange(event) {
        this.aadhaarFileType = false;
        this.aadhaarFileSize = false;
        this.aadhaarfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.aadhaarfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.aadhaarCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.aadhaarCertificateDoc = file;
                }
                else {
                    return this.aadhaarFileType = true;
                }
            }
            else {
                return this.aadhaarFileSize = true;
            }
        }
    }

    centralfileChange(event) {
        this.centralFileType = false;
        this.centralFileSize = false;
        this.centralfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.centralfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.centralCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.centralCertificateDoc = file;
                }
                else {
                    return this.centralFileType = true;
                }
            }
            else {
                return this.centralFileSize = true;
            }
        }
    }


    nccbfileChange(event) {
        this.nccbFileSize = false;
        this.nccbFileType = false;
        this.nccbfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.nccbfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.nccBCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.nccBCertificateDoc = file;
                }
                else {
                    return this.nccbFileType = true;
                }
            }
            else {
                return this.nccbFileSize = true;
            }
        }
    }

    ncccfileChange(event) {
        this.ncccFileType = false;
        this.ncccFileSize = false;
        this.nccfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.nccfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.nccCCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.nccCCertificateDoc = file;
                }
                else {
                    return this.ncccFileType = true;
                }
            }
            else {
                return this.ncccFileSize = true;
            }
        }
    }

    sportsfileChange(event) {
        this.sportsFileType = false;
        this.sportsFileSize = false;
        this.sportsfileRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.sportsfileRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.sportsManDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.sportsManDoc = file;
                }
                else {
                    return this.sportsFileType = true;
                }
            }
            else {
                return this.sportsFileSize = true;
            }
        }
    }

    highfileChange(event) {
        this.highFileType = false;
        this.highFileSize = false;
        this.highRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.highRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.highCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.highCertificateDoc = file;
                }
                else {
                    return this.highFileType = true;
                }
            }
            else {
                return this.highFileSize = true;
            }
        }
    }


    interfileChange(event) {
        this.interFileType = false;
        this.interFileSize = false;
        this.interRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.interRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.interCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.interCertificateDoc = file;
                }
                else {
                    return this.interFileType = true;
                }
            }
            else {
                return this.interFileSize = true;
            }
        }
    }

    addfileChange(event) {
        this.addFileType = false;
        this.addFileSize = false;
        this.additionalRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.additionalRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.additionalCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.additionalCertificateDoc = file;
                }
                else {
                    return this.addFileType = true;
                }
            }
            else {
                return this.addFileSize = true;
            }
        }
    }

    gradfileChange(event) {
        this.gradFileType = false;
        this.gradFileSize = false;
        this.gradRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.gradRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.ugCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.ugCertificateDoc = file;
                }
                else {
                    return this.gradFileType = true;
                }
            }
            else {
                return this.gradFileSize = true;
            }
        }
    }

    postGradfileChange(event) {
        this.postGradFileType = false;
        this.postGradFileSize = false;
        this.postGradRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.postGradRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.pgCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.pgCertificateDoc = file;
                }
                else {
                    return this.postGradFileType = true;
                }
            }
            else {
                return this.postGradFileSize = true;
            }
        }
    }

    techfileChange(event) {
        this.techFileType = false;
        this.techFileSize = false;
        this.degreeRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.degreeRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.degreeCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.degreeCertificateDoc = file;
                }
                else {
                    return this.techFileType = true;
                }
            }
            else {
                return this.techFileSize = true;
            }
        }
    }


    diplomafileChange(event) {
        this.diplomaFileType = false;
        this.diplomaFileSize = false;
        this.diplomaRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.diplomaRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.diplomaCertificateDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.diplomaCertificateDoc = file;
                }
                else {
                    return this.diplomaFileType = true;
                }
            }
            else {
                return this.diplomaFileSize = true;
            }
        }
    }


    signChange(event) {
        this.signType = false;
        this.signSize = false;
        this.signRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.signRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.browseSignatureDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.browseSignatureDoc = file;
                }
                else {
                    return this.signType = true;
                }
            }
            else {
                return this.signSize = true;
            }
        }
    }

    photoChange(event) {
        this.photoType = false;
        this.photoSize = false;
        this.photoRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.photoRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.browsePhotoDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.browsePhotoDoc = file;
                }
                else {
                    return this.photoType = true;
                }
            }
            else {
                return this.photoSize = true;
            }
        }
    }



    experienceChange(event) {
        this.expFileType = false;
        this.expFileSize = false;
        this.experienceRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.experienceRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            this.user.experienceCertificateDoc = file;
            // if (4200 < file.size && file.size < 204800) {
            //     if (matchType) {
            //         this.user.experienceCertificateDoc = file;
            //     }
            //     else {
            //         return this.expFileType = true;
            //     }
            // }
            // else {
            //     return this.expFileSize = true;
            // }
        }
    }



    drivingChange(event) {
        this.drivingFileType = false;
        this.drivingFileSize = false;
        this.drivingRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.drivingRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.drivingExperienceDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.drivingExperienceDoc = file;
                }
                else {
                    return this.drivingFileType = true;
                }
            }
            else {
                return this.drivingFileSize = true;
            }
        }
    }



    licenseChange(event) {
        this.licenseFileSize = false;
        this.licenseFileType = false;
        this.licenseRequired = false;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            this.licenseRequired = true;
            let file: File = fileList[0];
            let fileType = file.type.split('/')[1];
            let matchType = '|png|jpg|jpeg|'.indexOf(fileType) >= 1;
            // this.user.drivingLicenseDoc = file;
            if (4200 < file.size && file.size < 204800) {
                if (matchType) {
                    this.user.drivingLicenseDoc = file;
                }
                else {
                    return this.licenseFileType = true;
                }
            }
            else {
                return this.licenseFileSize = true;
            }
        }
    }


    adharChange(value: boolean) {
        if (value) {
            this.adharCheck = false;
        } else {
            this.adharCheck = true;
            this.user.adharCardNumber = '';
        }
    }

    communityChange(value: boolean) {
        if (value) {
            this.communityCheck = false;
        } else {
            this.communityCheck = true;
            this.user.communityName = 0;
        }
    }

    addressChange() {
        if (this.sameAddressCheck) {
            this.samePostalAddress = true;
        }
        else {
            this.samePostalAddress = false;
        }
    }

    centralChange(value: boolean) {
        if (value) {
            this.centralCheck = false;
        } else {
            this.centralCheck = true;
            this.user.centralGovt = '';
            this.user.servedFrom = '';
            this.user.servedTo = '';
        }
    }

    empExBroChange(value: boolean) {
        if (value) {
            this.empExBroCheck = false;
        } else {
            this.empExBroCheck = true;
            this.user.gsNumber = '';
            this.user.servedBroFrom = '';
            this.user.servedBroTo = '';
        }
    }

    exServiceChange(value: boolean) {
        if (value) {
            this.exServiceCheck = false;
        } else {
            this.exServiceCheck = true;
            this.user.armyNumber = '';
            this.user.armyRank = '';
        }
    }

    sonGREFChange(value: boolean) {
        if (value) {
            this.sonGREFCheck = false;
        } else {
            this.sonGREFCheck = true;
            this.user.sonGSNumber = '';
            this.user.sonRank = '';
            this.user.sonName = '';
        }
    }

    broGREFChange(value: boolean) {
        if (value) {
            this.broGREFCheck = false;
        } else {
            this.broGREFCheck = true;
            this.user.brotherArmy = '';
            this.user.brotherRank = '';
            this.user.brotherName = '';
        }
    }

    nccBChange(value: boolean) {
        if (value) {
            this.nccBCheck = false;
        } else {
            this.nccBCheck = true;
        }
    }

    nccCChange(value: boolean) {
        if (value) {
            this.nccCCheck = false;
        } else {
            this.nccCCheck = true;
        }
    }


    sportsChange(value: boolean) {
        if (value) {
            this.sportsCheck = false;
        } else {
            this.sportsCheck = true;
        }
    }

    degreeChange(value: boolean) {
        if (value) {
            this.degreeCheck = false;
        } else {
            this.degreeCheck = true;
        }
    }

    disableChange(value: boolean) {
        if (value) {
            this.disableCheck = false;
        } else {
            this.disableCheck = true;
            this.user.disablity = '';
        }
    }

    gradeChange(value: boolean) {
        if (value) {
            this.highGradeCheck = false;
        }
        else {
            this.highGradeCheck = true;

        }
    }
    iGradeChange(value: boolean) {
        if (value) {
            this.interGradeCheck = false;
        }
        else {
            this.interGradeCheck = true;

        }

    }




    public coursetypes: any = [
        { id: 1, name: "REGULAR", value: "REGULAR" },
        { id: 1, name: "PRIVATE", value: "PRIVATE" },
        { id: 1, name: "VOCATIONAL", value: "VOCATIONAL" },
    ]

    public courses: any = [
        { id: 1, name: "PRIVATE", value: "private" },
        { id: 2, name: "REGULAR", value: "regular" },
    ];

    public genders: any = [
        { id: 1, name: "MALE", value: "male" },
        { id: 2, name: "FEMALE", value: "female" },
    ];


    public communities: any = [
        { id: 0, name: "Muslim" },
        { id: 1, name: "Christians" },
        { id: 2, name: "Sikh" },
        { id: 3, name: "Budh" },
        { id: 4, name: "Zoroas(Parsis)" }
    ];

    public nationalities: any = [
        { id: 0, name: "Indian" },
        { id: 1, name: "Others" }
    ];

    public marital: any = [
        { id: 0, name: "MARRIED" },
        { id: 0, name: "UNMARRIED" }
    ];

    // public categories: any = [
    //     { id: 0, name: "GENERAL" },
    //     { id: 1, name: "SC" },
    //     { id: 2, name: "ST" },
    //     { id: 3, name: "OBC" },
    //     { id: 4, name: "OTHERS" }
    // ];
}
