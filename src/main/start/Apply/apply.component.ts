import { Component, ViewChild, OnInit } from '@angular/core';
import { GetService, AlertService, SharedService } from '../../Services/index';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'apply',
    templateUrl: './apply.html',
    providers: []
})


export class ApplyComponent implements OnInit {

    public loading = false;
    public activeJobs: any = [];
    
    constructor(private getService: GetService,
        private alertService: AlertService, private router: Router,
        private sharedService: SharedService) {

    }

    ngOnInit() {
        this.getActiveJobs();
    }

    getActiveJobs() {
        this.loading = true;
        this.getService.getActiveJobs().subscribe(
            data => {
                this.activeJobs = data;
                this.loading = false;
                this.alertService.success('Registration successful', true);
                // this.router.navigate(['/login']);
            },
            error => {
                this.loading = false;
                this.alertService.error(error);
                // this.loading = false;
            });
    }

    applyJob(jobs: any) {
        this.sharedService.saveData(jobs);
        this.router.navigate(['/candidate/register']);
    }

}