import { Component, ViewChild } from '@angular/core';
// import { GetService } from '../Services/get.service';
import { Http, Response } from '@angular/http';
import { Routes, RouterModule, Router } from '@angular/router';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
    selector: 'home',
    templateUrl: './home.html',
    styleUrls: ['./home.component.css'],
    providers: []
})


export class HomeComponent {
    
}