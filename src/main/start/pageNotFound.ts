import { Component, Injectable, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
   template: `
  <div class="container">
    <div class="row">
        <div class="col-sm-12">
			<br><br><br><br><br><br>
        	<br><br><br><br>
            <div class="error-template" style="padding: 40px 15px;text-align: center;">
                <h1  style="font-size:400%;">
                    Oops!</h1>
                <h2  style="color:red;">
                    404 Not Found</h2>
                <div class="error-details">
                    <b>Sorry, an error has occured, Requested page not found!</b>
                </div>
            </div>
        </div>
    </div>
</div>
`
})

export class PageNotFoundComponent {
}