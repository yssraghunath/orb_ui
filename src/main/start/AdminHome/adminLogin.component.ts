import { Component, ViewChild, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Http, Response } from '@angular/http';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AuthenticationService, AlertService, LoginService } from '../../Services/index';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { JhiEventManager } from 'ng-jhipster';
import { StateStorageService } from '../../shared/auth/index';


@Component({
    selector: 'adminLogin',
    templateUrl: './adminLogin.html',
    providers: []
})


export class AdminComponent {
    public admin: any = {};
    public returnUrl: string;
    public authenticationError: boolean;
    public loading = false;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public authenticationService: AuthenticationService,
        public alertService: AlertService,
        private eventManager: JhiEventManager,
        private loginService: LoginService,
        private stateStorageService: StateStorageService,
        private elementRef: ElementRef,
        private renderer: Renderer,
    ) {
    }

    loginAdmin() {
        this.loading = true;
        this.authenticationError = false;
        this.loginService.login({
            username: this.admin.username,
            password: this.admin.password,
        }).then(() => {
            this.loading = false;
            this.authenticationError = false;
            this.eventManager.broadcast({
                name: 'authenticationSuccess',
                content: 'Sending Authentication Success'
            });

            // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // // since login is succesful, go to stored previousState and clear previousState
            // const redirect = this.stateStorageService.getUrl();
            // if (redirect) {
            //     this.stateStorageService.storeUrl(null);
            //     this.router.navigate([redirect]);
            // }
            this.router.navigate(['/admin/job/post']);
        }).catch(() => {
            this.loading = false;
            this.authenticationError = true;
        });
    }
}