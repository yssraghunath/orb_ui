import { Component, ViewChild } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { Http, Response } from '@angular/http';
import { AuthenticationService, AlertService, LoginService, GetService, SharedService } from '../../../Services/index';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';


@Component({
    templateUrl: './jobPost.html',
    providers: []
})

export class JobPostComponent {
    public authenticationError: boolean;
    public loading = false;
    public successField: any = false;
    public jobList: any = [];

    public admin: any = [
        {
            jobTitle: '',
            jobDesc: ''
        }
    ];

    public newValue: any = {
        jobTitle: '',
        jobDesc: ''
    }

    pdfSrc: string = '../../assets/images/Amazon.pdf';
    constructor(public authenticationService: AuthenticationService, private router: Router,
        private getService: GetService, private sharedService: SharedService) {

    }

    ngOnInit() {
        this.getCreatedJobs();
    }

    saveJob() {
        this.loading = true;
        this.admin;
        this.successField = false;
        this.authenticationError = false;
        this.authenticationService.saveJob(this.admin).subscribe(data => {
            this.getCreatedJobs();
            this.jobList = data.json();
            if (data.status == 200) {
                this.successField = true;
                this.loading = false;
                this.authenticationError = false;
                
            }
            this.admin = [
                {   
                    
                    jobTitle: '',
                    jobDesc: ''
                }
            ];
        },
            error => {
                console.log("error saving data", error);
                this.loading = false;
                this.authenticationError = true;
            });
    }

    addPost() {
        this.loading = true;
        // this.admin.push(this.newValue);
        this.admin.push({postName:'', postDesc: ''});
    }

    removePost(index: any) {
        this.admin.splice(index, 1);
    }

    addFields() {
        
        this.router.navigate(['/admin/job/post']);
    }
    homePage() {
        this.router.navigate(['/home']);
    }


    addJobFields(job) {
    
        this.sharedService.saveJobData(job);
        this.router.navigate(['/admin/job/fields']);
    }

    removeJob(job) {
     
   
        this.authenticationService.removeJob(job).subscribe(
            data => {
                // console.log("post Course :", data);
                // this.jobList = data;
                this.getCreatedJobs();   
            },
            error => {
                // this.alertService.error(error);
                // this.loading = false;
            });

    }


    // // by Dheeraj    
    //  editJob(job) {
      
    //     this.sharedService.editJobData(job);
    //     this.router.navigate(['/admin/job/fields']);
    //  }


    getCreatedJobs() {
       
        this.getService.getCreatedJobs().subscribe(
            data => {
                // console.log("post Course :", data);
                this.jobList = data;
                // this.loading = false;
            },
            error => {
                // this.alertService.error(error);
                //  this.loading = false;
            });
    }
}