import { Component, ViewChild, AfterViewInit, Renderer, ElementRef } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Http, Response } from '@angular/http';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { Observable } from 'rxjs/Rx';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AuthenticationService, AlertService, LoginService } from '../../../Services/index';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { JhiEventManager } from 'ng-jhipster';
//import { StateStorageService } from '../../shared/auth/index';


@Component({
    templateUrl: './adminRegistration.html',
    providers: []
})


export class AdminRegistartionComponent {
    public admin: any = {};
    public returnUrl: string;
    public authenticationError: boolean;
    public loading = false;
    public successDoc: boolean;

    constructor(private route: ActivatedRoute,
        private router: Router, private authenticationService: AuthenticationService) {
            this.admin.zone = 0;
            this.admin.role = 0; 

    }
    adminRegistration() {
        this.loading = true;
        this.successDoc = false;
        this.authenticationService.adminRegister(this.admin).subscribe(data => {
            this.loading = false;
            this.successDoc = true;

        },
            error => {
                console.log("error saving data", error);
                this.loading = false;
                this.successDoc = false;
                this.authenticationError = true;
            });
    }

    public roles: any = [
        { id: 0, name: "Admin" },
        { id: 2, name: "User" }
    ];

    public zones: any = [
        { id: 0, name: "Pune" },
        { id: 1, name: "Rishikesh" },
        { id: 2, name: "Tezpur" }
    ];
}