import { NgModule, LOCALE_ID } from '@angular/core';
import { Title } from '@angular/platform-browser';

import {
    SharedLibsModule,
} from './';

@NgModule({
    imports: [
        SharedLibsModule
    ],
    declarations: [
    ],
    providers: [
        Title,
        {
            provide: LOCALE_ID,
            useValue: 'en'
        },
    ],
    exports: [
        SharedLibsModule,
    ]
})
export class SharedCommonModule {}
