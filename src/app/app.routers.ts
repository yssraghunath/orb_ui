import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from '../main/start/home.component';
import { ApplyComponent } from '../main/start/Apply/apply.component';
import { LoginComponent } from '../main/start/CandidateDetails/Login/login.component';
import { PaymentComponent } from '../main/start/CandidateDetails/Payment/payment';
import { ResetComponent } from '../main/start/CandidateDetails/PasswordReset/reset.component';
import { AboutComponent } from '../main/start/About/about.component';
import { ContactComponent } from '../main/start/Contact/contact.component';
import { ResultComponent } from '../main/start/Result/result.component';
import { AdminRegistartionComponent } from '../main/start/AdminHome/AdminRegistration/adminRegistration.component';
import { ExamComponent } from '../main/start/AdminHome/Exam/exam.component';
import { AdminComponent } from '../main/start/AdminHome/AdminLogin.component';
import { PersonalComponent } from '../main/start/CandidateDetails/Details/index';
import { UserRouteAccessService } from '../main/shared/auth/user-route-access-service';
import { PageNotFoundComponent } from '../main/start/pageNotFound';
import { JobPostComponent } from '../main/start/AdminHome/AdminJobPost/jobPost.component';
import { JobFieldComponent } from '../main/start/AdminHome/AdminJobFields/jobField.component';
import { PreviewComponent } from '../main/start/CandidateDetails/Preview/preview.component';
import { DetailsComponent } from '../main/start/AdminHome/DetailsCheck/detailCheck';
import { ReportComponent } from '../main/start/AdminHome/JobReport/report';

const appRoutes: Routes = [
    { path: 'candidate/login', component: LoginComponent },

    { path: 'home', component: HomeComponent },

    { path: 'apply', component: ApplyComponent },

    { path: 'password/reset', component: ResetComponent, data: {
            authorities: [],
            pageTitle: 'Password'
        },
        canActivate: [UserRouteAccessService]
    },

    { path: 'contact', component: ContactComponent },
    
    { path: 'admin/candidate/list', component: DetailsComponent },

    { path: 'result', component: ResultComponent },

    { path: 'about', component: AboutComponent },

    { path: 'admin/registration', component: AdminRegistartionComponent },

    { path: 'admin/login', component: AdminComponent },

    { path: 'candidate/register', component: PersonalComponent },

    { path: 'admin/job/post', component: JobPostComponent },

    { path: 'admin/job/exam', component: ExamComponent },

    { path: 'admin/job/fields', component: JobFieldComponent },

    { path: 'admin/job/report', component: ReportComponent },

    { path: 'candidate/login/preview', component: PreviewComponent },

    { path: 'candidate/payment', component: PaymentComponent },

    { path: '', redirectTo: 'home', pathMatch: 'full' },

    { path: '**', component: PageNotFoundComponent }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true });