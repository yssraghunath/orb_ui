import { Component, Renderer } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Routes, RouterModule, Router, ActivatedRoute } from '@angular/router';

@Component({
	moduleId: module.id,
	selector: 'app-header',
	template: `
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2" style="text-align:center; padding-left:90px;padding-top:15px;">
				<img src="../../assets/images/hill.png" (click)="home()" class="img-responsive clickPoint">
			</div>	
			
			<div class="col-sm-8">
				<h2 style="text-align:center;color:MidnightBlue;text-shadow: 6px 6px 3px lightgrey;font-family: -webkit-pictograph;"><b>BORDER ROADS ORGANIZATION</b></h2>
				<h3 style="text-align: center;color: #172d6b;font-family: cursive;">Creates, Connect & Cares...</h3>
				<h4 style="text-align: center;color:red;font-family: monospace;font-weight: bold;">GREF & Record Centre, Pune</h4>
			</div>
			
			<div class="col-sm-2"style="padding-top:15px;">
				<img src="../../assets/images/lion.png" (click)="home()" class="img-responsive clickPoint">
			</div>	
		</div>
    </div>
      `,
})

export class HeaderComponent {
	constructor(title: Title,  private router: Router) {}

	home(){

		this.router.navigate(['/home']);
	}
}