import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './app.routers';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header.component';
import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { BsDropdownModule, CarouselModule, ModalModule } from 'ngx-bootstrap';
import { HomeComponent } from '../main/start/home.component';
import { ModalComponent } from '../main/Modal/modal.component';
import { ApplyComponent } from '../main/start/Apply/apply.component';
import { LoginComponent } from '../main/start/CandidateDetails/Login/login.component';
import { ResetComponent } from '../main/start/CandidateDetails/PasswordReset/reset.component';
import { PreviewComponent } from '../main/start/CandidateDetails/Preview/preview.component';
import { AboutComponent } from '../main/start/About/about.component';
import { ContactComponent } from '../main/start/Contact/contact.component';
import { ResultComponent } from '../main/start/Result/result.component';
import { AdminRegistartionComponent } from '../main/start/AdminHome/AdminRegistration/adminRegistration.component';
import { ExamComponent } from '../main/start/AdminHome/Exam/exam.component';
import { AdminComponent } from '../main/start/AdminHome/AdminLogin.component';
import { JobPostComponent } from '../main/start/AdminHome/AdminJobPost/jobPost.component';
import { JobFieldComponent } from '../main/start/AdminHome/AdminJobFields/jobField.component';
import { PersonalComponent } from '../main/start/CandidateDetails/Details/index';
import { SharedModule } from '../main/shared/shared.module';
import { LoginService, AuthenticationService, AlertService, GetService, SharedService } from '../main/Services/index';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { PageNotFoundComponent } from '../main/start/pageNotFound';
import { DecimalPipe } from '@angular/common';
import { DatePickerModule } from 'angular-io-datepicker';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { DetailsComponent } from '../main/start/AdminHome/DetailsCheck/detailCheck';
import { ReportComponent } from '../main/start/AdminHome/JobReport/report';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { LoadingModule } from 'ngx-loading';
import { PaymentComponent } from '../main/start/CandidateDetails/Payment/payment';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [AppComponent, HomeComponent, LoginComponent, ModalComponent, ApplyComponent, HeaderComponent, ResultComponent, ContactComponent, AboutComponent, AdminComponent,AdminRegistartionComponent,ExamComponent,
    PersonalComponent, ResetComponent, PageNotFoundComponent, JobPostComponent, JobFieldComponent, PreviewComponent, DetailsComponent,
    PaymentComponent, ReportComponent],
  
    imports: [BrowserModule, routing, SharedModule, HttpModule, BsDropdownModule.forRoot(), ModalModule.forRoot(), FormsModule,
    ReactiveFormsModule, CarouselModule.forRoot(), RecaptchaModule.forRoot(), RecaptchaFormsModule, DatePickerModule,NgxPaginationModule,
    BsDatepickerModule.forRoot(), TabsModule.forRoot(), LoadingModule],

  providers: [LoginService, LocalStorageService, SessionStorageService, AuthenticationService,
    AlertService, GetService, DecimalPipe, SharedService],
  bootstrap: [AppComponent]
})


export class AppModule { }
