import { BroOrpPage } from './app.po';

describe('bro-orp App', () => {
  let page: BroOrpPage;

  beforeEach(() => {
    page = new BroOrpPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
